#!bin/sh
# Support finder v0.0.1
# Copyright (c) 2016 Shota Shimazu

SCRIPT_URL="https://creampuff.github.io/services/Creampuff/installer/support"

echo "Finding support..."
if [ -e install_tmp/ ]; then
      rm -rf tmp/
      mkdir tmp/
else 
      mkdir tmp/
fi
cd tmp
wget $SCRIPT_URL/support_list
source 