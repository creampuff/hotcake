Hotcake
---------
[![Build Status](https://travis-ci.org/Creampuff/hotcake.svg?branch=master)](Build Status) [![Code Climate](https://codeclimate.com/github/Creampuff/hotcake/badges/gpa.svg)](https://codeclimate.com/github/Creampuff/hotcake) [![Test Coverage](https://codeclimate.com/github/Creampuff/hotcake/badges/coverage.svg)](https://codeclimate.com/github/Creampuff/hotcake/coverage) [![Issue Count](https://codeclimate.com/github/Creampuff/hotcake/badges/issue_count.svg)](https://codeclimate.com/github/Creampuff/hotcake) [![MIT License](http://img.shields.io/badge/license-Apache-blue.svg?style=flat)](LICENSE)

> I'll replace OS installer, I aim easiest but functional installer.
However, our first issue is to develop new desktop shell, chocolate.
So, while chocolate shell is released in the stable branch, this project will not start actively.



A lightweight fast installer for Creampuff. 
It enables you to install Creampuff more easier and faster.
This is also replacement of current installer Ubiquity that is adopted by official Ubuntu family and other Debian based distributions.

### Features 
|         Functions            | Status |
|:-------------------------:|:--------:|
| Language Setting        | Not work |
| Partitioning                  | Not work |
| Easy user info setting  | Not work |
| Advanced installation  | Not work |


### License 
This program is licensed open source license.
Everyone can distribute and modify source code, see [LICENSE](LICENSE) for detail.



